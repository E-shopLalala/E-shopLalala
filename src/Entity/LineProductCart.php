<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LineProductCartRepository")
 */
class LineProductCart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     */
    private $products;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShoppingCart", inversedBy="lineProductCarts")
     */
    private $shoppingCarts;

    public function getId()
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(?int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getProducts(): ?product
    {
        return $this->products;
    }

    public function setProducts(?product $products): self
    {
        $this->products = $products;

        return $this;
    }

    public function getShoppingCarts(): ?ShoppingCart
    {
        return $this->shoppingCarts;
    }

    public function setShoppingCarts(?ShoppingCart $shoppingCarts): self
    {
        $this->shoppingCarts = $shoppingCarts;

        return $this;
    }

    public function incrementQuantity($n){

        $this->quantity += $n;

    }
}
