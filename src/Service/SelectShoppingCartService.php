<?php

namespace App\Service;

use App\Entity\ShoppingCart;
use App\Entity\Product;
use App\Entity\LineProductCart;
use App\Service\SelectShoppingCartService;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use PhpParser\Node\Scalar\MagicConst\Line;
use Doctrine\Common\Persistence\ObjectManager;

class SelectShoppingCartService
{
  private $user;
  private $em;

  public function __construct(TokenStorageInterface $tokenStorage, ObjectManager $em) {
    $this->user = $tokenStorage->getToken()->getUser();
  $this->em = $em;
  }

  // Gives a User his active shoppingCart or creates a new one and gives it to him.
  public function assignShoppingCart()
  {
    $user = $this->user;
    if ($user) {
      foreach ($user->getShoppingCarts() as $cart) {
       
        if ($cart->getIsActive()) {
          return $cart;
        }
      }
      
      $cart = new ShoppingCart();
      $user->addShoppingCart($cart);
      return $cart; 
      
    }
  }


  public function appendToLineProduct(Product $product, int $quantity = 1)
  {
    $shoppingCart = $this->assignShoppingCart();


    foreach ($shoppingCart->getLineProductCarts() as $lineProduct) {
    // On cherche dans le shoppingCart toutes les LineProductCart puis on verifie si le produit est déja présent. Si oui, quantité++, si non, nouvelle LineProduct avec le produit dedans. /!\ Prix

      if ($product->getId() === $lineProduct->getProducts()->getId()) {
          $lineProduct->setQuantity($lineProduct->getQuantity()+$quantity);
          $shoppingCart->addLineProductCart($lineProduct);
          $this->em->persist($shoppingCart);
          $this->em->flush();
          return;
      }
    }
    $lineProduct = new LineProductCart();
    $lineProduct->setQuantity(1);
    $lineProduct->setPrice($product->getPrice());
    $lineProduct->setProducts($product);
    $shoppingCart->addLineProductCart($lineProduct);
    $this->em->merge($shoppingCart);
    $this->em->flush();
  
    // Cree un nouveau lineProduct en récupérant l'id et la quantité de produit a travers un formulaire et en récupérant tout ca avec une Request (mm id ?).
    
  }

  public function validateShoppingCart(){
    $shoppingCart = $this->assignShoppingCart();
    $shoppingCart->setIsActive(false);
    $shoppingCart = $this->assignShoppingCart();
    $this->em->persist($shoppingCart);
    $this->em->flush();
  }

  public function getCurrentUser()
  {
    return $this->user;
  }

}