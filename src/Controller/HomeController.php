<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Service\SelectShoppingCartService;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Product;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index(SelectShoppingCartService $services, Request $request)
    {

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
}
