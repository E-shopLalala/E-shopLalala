<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\ShoppingCart;
use App\Service\SelectShoppingCartService;

class ShoppingCartController extends Controller
{
    /**
     * @Route("/shopping/cart/user", name="shopping_cart")
     */
    public function index(SelectShoppingCartService $services)
    {
        $cart = $services->assignShoppingCart();
        
    return $this->render('shopping_cart/index.html.twig', [
            'controller_name' => 'ShoppingCartController',
            'shopping_cart' => $cart
        ]);
    }

    /**
     * @Route("/shopping/cart/validation/user", name="validate_shopping_cart")
     */
    public function shoppingCartValidation(SelectShoppingCartService $services){
        $services->validateShoppingCart();
        return $this->redirectToRoute('full_page_product');

    }
}
