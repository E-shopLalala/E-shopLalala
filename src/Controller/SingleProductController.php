<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Product;


class SingleProductController extends Controller
{
    /**
     * @Route("/single/product/{id}", name="single_product")
     */
    public function index(Product $id)
    {
        
        return $this->render('single_product/index.html.twig', [
            'product' => $id
        ]);
        
    }
}
